USE bibliotheque;

-- la sous-requête retourne un seul résultat (1 colonne et 1 ligne)

-- obtenir les livres qui sortie la même année que l'année moyenne des livres policier
 SELECT titre, annee FROM livres
 WHERE annee=(
				SELECT Round(avg(annee),0) FROM livres -- 1956
				WHERE livres.genre=1
			 );
			
-- IN -> le résultat de la sous requête 1 colonne et 1 ou n lignes 
-- obtenir les livres qui sont sortie la même année qu'un livre d'horreur
SELECT titre,annee FROM livres 
WHERE annee IN(
				SELECT annee FROM livres
				WHERE genre=3
			)
ORDER BY annee;

-- ANY OU SOME

-- obtenir les livres dont l'année de sortie se trouve  après
-- les années de sortie des livres de science-fiction
SELECT titre,annee FROM livres
WHERE annee > ANY(
	SELECT annee FROM livres
				WHERE genre=6
	);

-- ALL
SELECT titre,annee FROM livres
WHERE annee > ALL(
	SELECT annee FROM livres
				WHERE genre=6
	);


-- EXISTS
SELECT genres.nom FROM genres
WHERE NOT EXISTS(
		SELECT id 
		FROM livres
		WHERE genre=genres.id
		);
		
SELECT titre,(SELECT nom FROM genres WHERE genres.id=livres.genre) 
FROM livres

-- group_concat -> concatener plusieurs lignes
SELECT titre , annee,
	(SELECT group_concat(concat_ws(' ',prenom,nom)  SEPARATOR ', ') FROM auteurs
	INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur 
	WHERE livre2auteur.id_livre= livres.id)
  FROM livres;

USE world;

SELECT country.name FROM country
WHERE capital in(SELECT city.id FROM city 
WHERE city.Population >1000000);

	