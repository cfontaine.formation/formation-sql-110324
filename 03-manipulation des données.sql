USE exemple;

-- Ajouter des données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO marques VALUES(1,'Marques A','1975-06-10');

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO marques(nom,date_creation) VALUES ('Marques B','1980-01-01'); 
INSERT INTO marques (nom) VALUES ('Marques C');

-- Erreur -> nom ne peut pas être NULL
-- INSERT INTO marques (date_creation) VALUES ('1980-01-01');

-- Insérer Plusieurs lignes
INSERT INTO articles(reference , nom_article, prix,marque) VALUES 
(1000,'Tv',500.0,1),
(200,'Souris',20.0,3),
(201,'Clavier AZERTY',15.0,3),
(2000,'TV 4K OLED',2000.0,1),
(202,'Cable HDMI',9.0,3),
(3000,'Disque SSD 1Go',100.0,4);

-- Insérer des données dans une relation n-n
INSERT INTO fournisseurs (nom) VALUES 
('fournisseur 1'),
('fournisseur 2');

-- Insérer des données dans une relation n-n
INSERT INTO fournisseurs_articles (id_articles,id_fournisseurs) VALUES 
(1000,1),
(1000,3),
(200,1),
(201,1),
(2000,1),
(2000,2),
(2000,3);

-- Supprimer des données => DELETE, TRUNCATE
-- Supression de la ligne qui a pour reference 3000 dans la table articles
DELETE FROM articles WHERE reference=3000;

INSERT INTO articles(reference , nom_article, prix,marque) VALUES 
(1001,'Tv',5000.0,1),
(206,'Souris Gamer',40.0,3),
(205,'Ecran 32',3500.0,3);

-- Supprimer toutes les lignes de la table articles qui ont un prix > 2200
DELETE FROM articles WHERE prix>2200;

-- Supprimer toutes les lignes de la table fournisseurs_articles (il n'y a pas de condition)
DELETE FROM fournisseurs_articles;

-- Valeur auto_increment et la suppression
-- DELETE => ne ré-initialise pas la colonne AUTO_INCREMENT
DELETE FROM fournisseurs ;

INSERT INTO fournisseurs (nom) VALUES 
('Fournisseur A'),
('Fournisseur B'),
('Fournisseur c');
-- TRUNCATE => réinitailise l'auto incrément
SET FOREIGN_KEY_CHECKS=0;	-- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table articles  => remet la colonne AUTO_INCREMENT à 0
TRUNCATE TABLE fournisseurs;
SET FOREIGN_KEY_CHECKS=1;	-- réactiver la vérification des clés étrangères

INSERT INTO fournisseurs (nom) VALUES 
('Fournisseur A'),
('Fournisseur B'), 
('Fournisseur c');

-- Modification des données => UPDATE
-- Modification du prix et de la description de l'article qui a pour reference 201
UPDATE articles SET nom_article = 'Clavier querty' WHERE reference= 201;

-- Modification de la marque à NULL de l'article qui a un prix égal à 201
UPDATE articles SET marque = NULL WHERE reference= 201;

UPDATE articles SET nom_article = 'Clavier QWERTY', prix= 5.90 WHERE reference= 201;

UPDATE articles SET prix =5.0 WHERE prix<30.0;

-- Modifier tous les prix supérieur à 100.0 en les augmentant de 5%
UPDATE articles SET prix=prix*1.05 WHERE prix>100.0;

-- Pas de condition -> tous les prix passe à 1.0
UPDATE articles SET prix=1.0 ;

-- Les contraintes de clé étrangère
-- INSERT INTO articles(reference , nom_article, prix,marque) VALUES 
(2,'stylo',4.5,15);

-- DELETE FROM marques WHERE id=1;
DELETE FROM articles WHERE marque =1;

DELETE FROM marques WHERE id=1;

USE pizzeria;

INSERT INTO ingredients (nom) VALUES 
('mozarella'),
('champginon'),
('jambon'),
('anchois'),
('tomate');

INSERT INTO pizzas(nom, base,prix) VALUES 
('margarita','rouge',11.0),
('forestière','rouge',12.0),
('napolitaine',1,15.0);

INSERT INTO pizzas_ingredients (numero_ingredient, numero_pizza) VALUES 
(5,1), -- margarita
(1,1),
(5,2), -- forestière
(2,2),
(3,2),
(5,3), -- napolitaine
(4,3);

UPDATE pizzas SET prix=13.5 WHERE numero_pizza=3; 

UPDATE pizzas SET prix=prix*1.2;

DELETE FROM pizzas_ingredients WHERE numero_pizza =2;
DELETE FROM pizzas WHERE numero_pizza =2;
