-- Sélectionner la base des données bibliotheque
USE bibliotheque;

-- Obtenir le titre et l'année de sortie de tous les livres
SELECT titre,annee FROM livres;

-- * -> pour obtenir toutes les colonnes d'une table ici livres
SELECT * FROM livres;

-- Quand il ya une ambiguitée sur le nom d'une colonne (le même nom présent dans plusieurs tables),
-- on ajoute le nom de la table => nom_table.nom_colonne
SELECT auteurs.id, prenom, pays.nom FROM auteurs,pays;

-- On peut mettre dans les colonnes d'un SELECT: une constante ou une colonne qui provient d'un calcul
SELECT titre, 'age=', 2024 - annee FROM livres;

-- DISTINCT -> permet d’éviter les doublons dans les résultats d’un SELECT
SELECT ALL prenom FROM auteurs; -- 39 lignes
SELECT prenom FROM auteurs; 	-- ALL est implicite
SELECT DISTINCT prenom FROM auteurs; -- 37 lignes

-- On obtient tous les auteurs qui ont un prenom + un nom distinct 
SELECT DISTINCT prenom,nom FROM auteurs;

-- ALias 
-- un alias permet de renommer temporairement une colonne ou une table dans une requête
-- colonne AS alias, AS est optionel => colonne Alias 
-- sur les colonnes
SELECT titre AS titre_livre,  2024 - annee AS age FROM livres;
SELECT titre titre_livre,  2024 - annee age FROM livres;

-- Sur les tables
SELECT a.prenom, a.nom FROM auteurs AS a ;
SELECT a.prenom, a.nom FROM auteurs a ;

-- Clause WHERE
-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous les titres de livre qui sont sortie après 2000
SELECT titre, annee FROM livres WHERE annee>2000;
SELECT titre, annee FROM livres WHERE 2024-annee>50;

-- Opérateurs logiques
-- Les opérateurs logiques AND et OR permettent de combiner des conditions

-- condition 1 | condition 2 | AND | OR | XOR
--    V  	   |     V       | V   | V  | F
--    F  	   |     V       | F   | V  | V
--    V  	   |     F       | F   | V  | V
--    F  	   |     F       | F   | F  | F

-- avec l'opérateur AND, il faut que toutes les conditions soient vrai pour que la ligne soit sélectionnée
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1960 et 1990
SELECT titre,annee FROM livres WHERE annee>1960 AND annee<1990;
-- Selection de tous les titres , année de sortie et genre de livre qui sont sortie avant 1970 ET sont des livres policiers
SELECT titre,annee FROM livres WHERE annee <1970 AND genre=1;

-- avec l'opérateur OR, il faut qu'une des conditions soient vrai ou les 2 pour que la ligne soit sélectionnée
-- Selection de tous les titres et année de sortie de livre qui sont sortie en 1992 et en 1954
SELECT titre,annee FROM livres WHERE annee=1992 OR annee=1954;
-- Selection de tous les titres , année de sortie et genre de livre qui sont sortie avant 1912 OU sont des livres policiers ou les 2
SELECT titre,annee,genre  FROM livres WHERE annee<1912 OR genre=1;

-- L'opérateur XOR ou exclusif ->  il faut qu'une des conditions soient vrai pour que la ligne soit sélectionnée
-- Selection de tous les titres , année de sortie et genre de livre qui sont sortie avant 1912 OU sont des livres policiers mais pas les 2
SELECT titre,annee,genre FROM livres WHERE annee <1912 XOR genre=1;

-- L'opérateur NOT -> inverser la condition
SELECT titre , annee FROM livres WHERE NOT annee>2000

USE world;
-- Exercice requête simple

-- Afficher toutes les colonnes et toutes les lignes de la table countrylanguage
SELECT * FROM countrylanguage; 

-- Afficher le nom des villes et leur population
SELECT name,population FROM city; 

-- Afficher la liste des noms des continents sans doublons qui se trouve de la table country
SELECT DISTINCT continent FROM country;

-- Afficher le nom des villes du district de Nagano
SELECT name FROM city WHERE District ='Nagano';

-- Afficher les pays dont la surface est comprise entre 80000 et 100000 km2
SELECT name  FROM country WHERE SurfaceArea >=80000 AND SurfaceArea <=100000;

-- Afficher les noms et la population des villes qui ont un code_country égal à ITA et dont la population est supérieur à 300000 habitants
SELECT name,population FROM city WHERE CountryCode = 'ITA' AND Population >300000;

-- Afficher le nom du pays, le continent, espérance de vie et produit national brut (gnp) soit des pays qui ont une espérance de vie supérieur à 80 ans ou les pays européens qui ont un produit national brut supérieur à 1000000 d'euro
SELECT name,continent ,LifeExpectancy, gnp  FROM country
WHERE  LifeExpectancy>80 OR (Continent='Europe' AND gnp> 1000000);

USE bibliotheque;

-- l'opérateur IN permet de vérifier, si une colonne fait partie des valeurs d'un ensemble de valeurs définis
-- Sélection du titre et de l'année pour les livres sortie en 1992,1954 ou 1982
SELECT titre, annee FROM livres WHERE annee IN (1992,1954,1982);

-- Sélection du prénom et du nom pour les auteurs qui ont pour prénom Pierre, Jack, James
SELECT prenom, nom FROM auteurs WHERE prenom IN ('Pierre','Jack','James');

-- l'opérateur BETWEEN est utilisé pour vérifier si une colonne fait partie d’un intervalle de données
-- Sélection des titres et l'année des livres qui sortie entre 1970 et 1980
SELECT titre, annee FROM livres WHERE annee BETWEEN 1970 AND 1980;

-- Sélection du prénom , du nom  et de la date de naissance des auteurs qui sont nés entre le 1er janvier 1930 et  le 1er janvier 1950
SELECT prenom, nom,naissance  FROM auteurs WHERE naissance BETWEEN '1930-01-01' AND '1950-01-01';

-- Sélection du prénom , du nom des auteurs dont le prénom est compris entre John et Pierre
SELECT prenom, nom FROM auteurs WHERE prenom BETWEEN 'John' AND 'Pierre';

-- L’opérateur LIKE permet d’effectuer une recherche sur un modèle particulier
--  % représente 0,1 ou plusieurs caratères inconnues
--  _ représente un caratère inconnue

-- Sélection des titres des livres qui commence par d et qui fait 4 caractères
SELECT titre FROM livres WHERE titre LIKE 'd___';

-- Sélection des titres des livres qui commence par d et qui fait au moins 4 caractères
SELECT titre FROM livres WHERE titre LIKE 'd___%';

-- Sélection des titres des livres qui finie par s
SELECT titre FROM livres WHERE titre LIKE '%s';

-- Sélection deds titres des livres qui contiennent un espace
SELECT titre FROM livres WHERE titre LIKE '% %';

-- Sélection du prénoms pour les auteurs qui ont un prénom composé
SELECT prenom, nom FROM auteurs WHERE prenom LIKE '%-%';

-- Avec Mysql /mariadb -> collation par défaut utf8mb4_general_ci;
-- fini par ci -> case insensitive
-- fini par cs -> case sensitive

-- On peut définir la collation à la création d'une table avec COLLATE 
-- CREATE TABLE article(
-- 	reference INT,
-- 	description VARCHAR(255),
-- 	prix DECIMAL(6,2)
-- ) COLLATE=utf8mb4_general_ci;

-- IS NULL/IS NOT NULL
-- IS NOT NULL permet de tester, si une valeur est différente de NULL
-- Sélection du le prenom et le nom de la table auteurs pour les auteurs qui sont décédés
SELECT prenom, nom FROM auteurs WHERE deces IS NULL;

-- IS NOT NULL permet de tester, si une valeur est différente de NULL
-- Sélection du le prenom et le nom de la table auteurs pour les auteurs qui sont décédés
SELECT prenom, nom FROM auteurs WHERE deces IS NOT NULL;

-- Trier
-- ORDER BY -> trier par ordre ascendant ASC (par défaut), ou par ordre décendant DESC
-- Selection de tous les livres triés par rapport à l'année par ordre décroissant, au genre par ordre croissant et au titre par ordre croissant
SELECT id,titre,annee,genre FROM livres ORDER BY annee  ; -- annee ASC
SELECT id,titre,annee,genre FROM livres ORDER BY annee DESC ;
SELECT id,titre,annee,genre FROM livres ORDER BY genre DESC, annee, titre DESC;

SELECT titre,annee,genre FROM livres WHERE annee BETWEEN 1950 AND 1980 ORDER BY annee,titre;

-- LIMIT -> limiter le nombre de ligne du résultat
-- les 5 premiers livres de la table livres
SELECT titre FROM livres LIMIT 5;

-- les 3 livres les plus ancien
SELECT titre,annee FROM livres ORDER BY annee LIMIT 3  ;

-- OFFSET -> le nombre de ligne ignorer à partir du début
-- les 5 livres de la table livres à partir du 4ème
SELECT titre,annee FROM livres LIMIT 5 OFFSET 3 ;

-- avec Mysql/ mariadb, on peut aussi utiliser la syntaxe LIMIT decalage, nombre_de_ligne 
SELECT titre,annee FROM livres ORDER BY annee  LIMIT 2,5; 

USE world;

-- Afficher le nom et l'année d'indépendance des pays qui sont devenus indépendant en 1825, 1867, 1963 et  1993
SELECT name,indepyear FROM country WHERE IndepYear IN (1825, 1867, 196, 1993);

-- Afficher le nom, le continent et la population des pays commençant par la lettre c et contenant au moins 6 caractères, classé par ordre décroissant de population :
SELECT name, continent, population FROM country WHERE name LIKE 'c_____%' ORDER BY Population DESC; 

-- Afficher les 10 pays les plus peuplés (nom et population)
SELECT name, population FROM country ORDER BY population DESC LIMIT 10;

-- Afficher le nom de la 3 ème ville la plus peuplé
SELECT name FROM city ORDER BY population DESC LIMIT 1 OFFSET  2;

-- Afficher les pays qui n'ont pas de capital
SELECT name FROM country WHERE capital IS NULL;
