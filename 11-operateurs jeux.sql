USE exemple;

CREATE TABLE employees(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_personnel VARCHAR(255),
	salaire_mensuel double,
	temps_travail_semaine INT
);


CREATE TABLE clients(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_livraison VARCHAR(255),
	adresse_personnel VARCHAR(255)
);

INSERT INTO employees(prenom,nom, adresse_personnel,salaire_mensuel,temps_travail_semaine)
VALUES
('John','Doe','1,rue esquermoise Lille',1800,35),
('Jane','Doe','1,rue esquermoise Lille',2000,35),
('Jo','Dalton','46,rue des Cannoniers  Lille',2800,40),
('Alan','Smithee','32 Boulevard Vincent Gache Nantes',2300,37),
('Yves','Roulo','1,46,rue des Cannoniers  Lille',1400,20);

INSERT INTO Clients(prenom,nom,adresse_livraison , adresse_personnel)
VALUES
('John','Doe','1,rue esquermoise Lille','1,rue esquermoise Lille'),
('Pierre','Martin','23,rue esquermoise Lille','23,rue esquermoise Lille'),
('Bastien','Dupond','46,rue des Cannoniers  Lille','46,rue des Cannoniers  Lille');

SELECT * FROM clients;

SELECT * FROM employees;

-- UNION -> suprimme les doublons
SELECT prenom,nom,adresse_personnel FROM employees
UNION
SELECT prenom_client,nom,adresse_personnel FROM clients;

-- UNION ALL -> autorise les doublons
SELECT prenom,nom,adresse_personnel FROM employees
UNION ALL
SELECT prenom_client,nom,adresse_personnel FROM clients;

SELECT prenom,nom,adresse_personnel,'employe' AS identifiant FROM employees
UNION
SELECT prenom_client,nom,adresse_personnel,'client' AS identifiant FROM clients;

-- INTERSECT
SELECT prenom,nom,adresse_personnel FROM employees
INTERSECT 
SELECT prenom_client,nom,adresse_personnel FROM clients;

-- EXCEPT
SELECT prenom,nom,adresse_personnel FROM employees
EXCEPT 
SELECT prenom_client,nom,adresse_personnel FROM clients;

USE world;

SELECT city.population, city.name , 'ville' AS lieu FROM city WHERE city.population<1000
union
SELECT country.population, country.name , 'pays' AS lieu FROM country WHERE country.population<1000;
