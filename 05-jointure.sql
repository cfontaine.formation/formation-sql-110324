USE bibliotheque;

-- Jointure interne

-- Avec WHERE
SELECT livres.id,livres.titre,livres.genre,genres.id,genres.nom
FROM livres,genres
WHERE livres.genre=genres.id;

-- Avec INNER JOIN

-- Relation 1,N
-- On va joindre la table livres et la table genres en utilisant l'égalité entre
-- La clé primaire de genres -> id et la clé étrangère de livre -> genre
SELECT livres.id,livres.titre,livres.genre,genres.id,genres.nom
FROM livres
INNER JOIN genres ON livres.genre=genres.id;

SELECT livres.titre,genres.nom
FROM livres
INNER JOIN genres ON livres.genre=genres.id;

-- Afficher le prénom et le nom de l'auteur et le nom du pays
SELECT prenom , auteurs.nom, pays.nom 
FROM auteurs
INNER JOIN pays ON auteurs.nation = pays.id;

-- Afficher le titre, l'année de sortie et le genre du livre qui sont sortie entre 1980 et 1990
-- pour les genres policier et cyberpunk
SELECT titre,annee,genres.nom
FROM livres
INNER JOIN genres ON livres.genre=genres.id
WHERE annee BETWEEN 1980 AND 1990 AND genres.nom IN('policier','cyberpunk')
ORDER BY annee;

-- Relation n-n
-- On va joindre : la table livres et la table de jointure livre2auteur
-- On va joindre le resultat de la jointure précédente avec la table auteurs
SELECT titre ,annee, prenom, auteurs.nom FROM livres
INNER JOIN livre2auteur ON livres.id = livre2auteur.id_livre 
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur ;

SELECT titre ,annee, genres.nom, prenom, auteurs.nom  
FROM livres
INNER JOIN livre2auteur ON livres.id = livre2auteur.id_livre 
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur
INNER JOIN genres ON livres.genre = genres.id
WHERE annee>1970 
ORDER BY annee DESC;

-- Exercice Jointure interne
USE world;
-- Afficher chaque nom de pays et le nom de sa capitale
SELECT country.name,city.name 
FROM city 
INNER JOIN country  
ON country.Capital =city.Id;

-- Afficher les nom de pays et le nom de ses villes classer par nom de pays puis par nom de ville en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name,city.name FROM city 
INNER JOIN country 
ON city.CountryCode =Code 
WHERE city.name IS NOT NULL
ORDER BY  country.name,city.name;

-- Afficher: les noms de pays, la langue et le pourcentage classé par pays et par pourcentage décroissant
SELECT country.name, countrylanguage.Language ,countrylanguage.Percentage FROM country
INNER JOIN countrylanguage
ON country.Code = countrylanguage.CountryCode 
ORDER BY country.Name , countrylanguage.Percentage DESC;

-- Produit cartésien -> CROSS JOIN
-- On obtient toutes les combinaisons possible entre les 2 tables
-- Le nombre d'enregistrements obtenu est égal au nombre de lignes de la première table multiplié par le nombre de lignes de la deuxième table.
USE exemple;

CREATE TABLE plats
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO plats(nom) VALUES
('Ceréale'),
('Pain'),
('Oeuf sur le plat');

CREATE TABLE boissons
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO boissons(nom) VALUES
('thé'),
('café'),
('jus d''orange');


-- On obtient toutes les combinaisons possible entre les boissons et les plats
SELECT plats.nom, boissons.nom FROM plats
CROSS JOIN boissons;

-- Jointure externe: LEFT JOIN ou RIGTH JOIN
USE bibliotheque;

SELECT nom,titre,annee FROM genres
LEFT JOIN livres ON genres.id= livres.genre;

-- équivalent avec un right join
SELECT nom,titre,annee FROM livres
RIGHT JOIN genres ON genres.id= livres.genre;

-- Obtenir tous les genres qui ne sont pas représentés dans la bibliotheque
SELECT genres.nom , titre FROM genres 
LEFT JOIN livres
ON livres.genre = genres.id
WHERE livres.id IS NULL;

SELECT genres.nom , titre FROM livres 
RIGHT JOIN genres
ON livres.genre = genres.id;

SELECT genres.nom , titre FROM livres 
RIGHT JOIN genres
ON livres.genre = genres.id
WHERE livres.id IS NULL;

-- Obtenir tous les auteurs dont la nationalité n'a pas été renseigné
SELECT prenom, auteurs.nom, pays.nom FROM auteurs
LEFT JOIN pays
ON pays.id=auteurs.nation
WHERE pays.id IS NULL;

-- Obtenir tous les pays qui n'ont d'auteur
SELECT p.nom FROM auteurs a -- utilisation des alias
RIGHT JOIN pays p
ON p.id=a.nation 
WHERE a.id IS NULL;

-- FULL JOIN
-- SELECT a.prenom,a.nom,p.nom AS nationalite  FROM auteurs a
-- FULL JOIN pays AS p ON a.nation = p.id ;

-- FULL JOIN n'est pas encore supporté par MySql/MariaDb
-- Mais on peut le réaliser avec cette requète.
SELECT prenom, auteurs.nom, pays.nom FROM auteurs
LEFT JOIN pays
ON pays.id=auteurs.nation
UNION 
SELECT prenom, a.nom,p.nom FROM auteurs a
RIGHT JOIN pays p
ON p.id=a.nation;

-- Jointure naturelle -> uniquement mysql et mariadb
-- la clé primaire et la clé étrangère doivent avoir le même nom

-- Changer le nom de la clé primaire  id -> genre
ALTER TABLE livres DROP CONSTRAINT fk_genres;
ALTER TABLE genres CHANGE id genre INT;

ALTER TABLE livres ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES  genres(genre);

-- Jointure naturelle
SELECT titre, genres.nom FROM livres
NATURAL JOIN genres;

-- Changer le nom de la clé primaire genre -> id
ALTER TABLE livres DROP CONSTRAINT fk_genres;
ALTER TABLE genres CHANGE genre id INT;

ALTER TABLE livres ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES  genres(id);

-- Exercice: Jointure interne et externe
USE world;
-- Afficher: les noms de  pays, la  langue et le  pourcentage  classé par pays et par  pourcentage décroissant. mais on ne veut obtenir que les langues officielles
SELECT name, LANGUAGE ,percentage  FROM country
INNER JOIN countrylanguage 
ON country.code = countrylanguage.CountryCode 
WHERE countrylanguage.IsOfficial ='T'
ORDER BY country.name, countrylanguage.percentage DESC;

-- Afficher le nom des pays sans ville
SELECT country .name FROM country 
LEFT JOIN city ON city.countrycode=country.code
WHERE city.name IS NULL;

-- Afficher tous les pays qui parlent français
SELECT country.name FROM country 
INNER JOIN countrylanguage ON country.code= countrylanguage.CountryCode 
WHERE countrylanguage.LANGUAGE='French'; 

USE elevage;
-- Afficher la liste des races de chiens qui sont des chiens de berger
SELECT race.nom FROM race
INNER JOIN espece ON race.espece_id =espece.id
WHERE espece.nom_courant='Chien' AND race.nom LIKE '%Berger%';

-- Afficher la liste des chats et des perroquets amazones, avec  leur espèce (nom latin) et leur race s'ils en ont une
-- Regroupez les chats ensemble, les perroquets ensemble et, au sein de l'espèce, regroupez les races.
SELECT espece.nom_latin, race.nom, animal.nom FROM espece 
LEFT JOIN race ON espece.id=race.espece_id
INNER JOIN animal ON espece.id=animal.espece_id
WHERE espece.nom_latin IN ('Felis silvestris','Alipiopsitta xanthops')
ORDER BY espece.nom_latin, race.nom;

SELECT espece.nom_latin, race.nom, animal.nom FROM espece 
LEFT JOIN race ON espece.id=race.espece_id
INNER JOIN animal ON espece.id=animal.espece_id
WHERE espece.nom_courant IN ('Chat','Perroquet amazone')
ORDER BY espece.nom_latin, race.nom;

-- Auto jointure
-- Une table que l'on joindre avec elle même

-- Une auto joiture est souvent utilisé pour représenter
-- une hierarchie en SQL (relation parent->enfant)
USE exemple;
CREATE TABLE salaries(
	id int PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	manager int ,
	
	CONSTRAINT fk_manager
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries(prenom,nom,manager) VALUES  ('John','Doe',NULL);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jane','Doe',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jo','Dalton',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Alan','Smithee',2);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Yves','Roulo',2);


SELECT employes.prenom, employes.nom, managers.prenom , managers.nom
FROM salaries AS employes
LEFT JOIN salaries AS managers ON employes.manager=managers.id

UPDATE salaries SET manager =5 WHERE id=1;
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Yves','Roulo',2);

-- Auto-jointure
USE elevage;

-- Afficher  la liste des enfants de Bouli
SELECT pere.nom AS nom_parent,enfant.nom  AS nom_enfant 
FROM animal AS pere 
LEFT JOIN animal AS enfant 
ON pere.id= enfant.pere_id 
WHERE pere.nom='Bouli';

-- Afficher la liste des chats dont on connaît les parents, ainsi que le nom de ces parents
SELECT enfant.nom  AS enfant,pere.nom AS pere, mere.nom AS mere
FROM animal AS pere 
LEFT JOIN animal AS enfant  ON pere.id= enfant.pere_id 
LEFT JOIN animal AS mere  ON mere.id= enfant.mere_id 
INNER JOIN espece ON enfant.espece_id = espece.id
WHERE espece.nom_courant = 'Chat';

SELECT  animal.race_id,  animal.nom, espece.nom_courant, animal.pere_id, animal.mere_id
FROM espece,  animal
WHERE  espece.id=animal.espece_id AND espece.nom_courant='chat' AND (animal.pere_id IS NOT NULL OR animal.mere_id IS NOT NULL)