USE bibliotheque;


-- Regroupement avec Group By
-- Obtenir le nombre de livre par année
SELECT annee,count(annee) FROM livres GROUP BY annee;

-- Obtenir le nombre de livre par genre
SELECT genres.nom, genre,count(livres.id) AS nb_livre FROM livres
INNER JOIN genres ON genres.id = livres.genre
GROUP BY genre;

-- having -> condition (idem where) après le regroupement, Where se trouve toujours avant le regroupement
SELECT genres.nom, count(livres.id) AS nb_livre FROM livres
INNER JOIN genres ON genres.id=livres.genre
GROUP BY livres.genre HAVING  nb_livre >=5 ORDER BY nb_livre DESC;

-- Obtenir le nombre de livre par auteur
SELECT CONCAT_ws(' ',prenom,nom), count(livres.id) AS nb_livre FROM livres
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id 
INNER JOIN auteurs ON livre2auteur.id_auteur= auteurs.id 
GROUP BY auteurs.id 
ORDER BY nb_livre DESC;

-- Obtenir le nombre de livre par genre pour le livres sortie après 1960 
-- pour les auteurs qui ont écrit au moins 10 livres
SELECT CONCAT_ws(' ',prenom,nom), count(livres.id) AS nb_livre FROM livres
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id 
INNER JOIN auteurs ON livre2auteur.id_auteur= auteurs.id 
WHERE annee >1960
GROUP BY auteurs.id 
HAVING nb_livre>3;
ORDER BY nb_livre DESC;

-- Exercice :Regroupement Group By
USE world;
-- Nombre de pays par continent
SELECT continent, count(code) FROM country GROUP BY continent;

-- les continents et leur  population totale classé du plus peuplé au moins peuplé
SELECT continent, sum(population) AS sum_population FROM country 
GROUP BY continent
ORDER BY sum_population DESC;

-- Nombre de langue officielle par pays - classé par nombre de langue officielle
SELECT * FROM country 
INNER JOIN countrylanguage
ON country.code= countrylanguage.CountryCode;

SELECT country.name, count(language)
FROM countrylanguage, country 
WHERE country.code=countrylanguage.CountryCode AND isofficial = 'T'
GROUP BY country.name;

-- fonction fenetrage

-- Création de la table ventes et ajout de données
USE exemple;

CREATE TABLE ventes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom_vendeur VARCHAR(30),
	annee INT,
	vente double
);

INSERT INTO ventes(nom_vendeur,annee,vente)
VALUES 
('Paul',2020,10000),
('Patrick',2020,11000),
('Paola',2020,15000),
('Paul',2021,8000),
('Patrick',2021,10000),
('Paola',2021,5000),
('Paul',2022,14000),
('Patrick',2022,5000),
('Paola',2022,16000),
('Paul',2019,12000),
('Patrick',2019,10000),
('Paola',2019,4000),
('Paul',2018,8000),
('Patrick',2018,10000),
('Paola',2018,5000),
('Paul',2017,1000),
('Patrick',2017,5000),
('Paola',2017,6000);


-- Clause over
-- OVER() -> la partition est la table complète
SELECT annee,nom_vendeur,vente, Sum(vente) OVER() FROM ventes;

-- regroupement sur les années
-- avec un group by
SELECT nom_vendeur ,  sum(vente)  AS somme_vente   FROM ventes GROUP BY nom_vendeur;

-- avec OVER(PARTITION BY )
-- OVER(PARTITION BY annee) -> les partitions correspondent aux annees
SELECT annee,nom_vendeur,vente, Sum(vente) OVER(PARTITION BY annee) FROM ventes;

SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY annee) AS somme_vente_annee,
sum(vente) OVER() AS somme_total,
round(avg(vente) OVER(PARTITION  BY annee),2) AS moyenne_vente_annee
FROM ventes;

-- OVER(PARTITION BY nom_vendeur) -> les partitions correspondent à nom_vendeur
SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

SELECT annee,nom_vendeur,vente, Sum(vente) 
OVER(PARTITION BY nom_vendeur ORDER BY vente DESC,annee)
FROM ventes;

-- Fonction de fenetrage
-- 1) Fonction d'agrégation: Min,Max,Sum,Avg,Count
SELECT annee,nom_vendeur,vente, 
Sum(vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

-- 2 Fonction de fenetrage de classement, il faut utiliser ORDER BY
-- ROW_NUMBER() -> numérote 1, 2, ... les lignes de la partitions
SELECT annee,nom_vendeur,vente, 
ROW_NUMBER() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY,
--           mais on aura des écarts dans la séquence des valeurs lorsque plusieurs lignes ont le même rang
-- 1 - 2 - 3 - 4 - 4 - 6
SELECT annee,nom_vendeur,vente, 
RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- DENSE_RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY
-- 1 - 2 - 3 - 4 - 4 - 5
SELECT annee,nom_vendeur,vente, 
DENSE_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;


-- NTILE(n)-> Distribue les lignes de chaque partition de fenêtre dans un nombre spécifié de groupes clas
SELECT annee,nom_vendeur ,vente,
NTILE(3) OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- PERCENT_RANK() -> Calculer le rang en pourcentage (rank - 1)/(rows - 1)
SELECT annee,nom_vendeur,vente, 
percent_rank() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- CUME_DIST -> Calcule la distribution cumulée d'une valeur dans un ensemble de valeurs
SELECT annee,nom_vendeur,vente, 
CUME_DIST() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- 3) Fonction de fenetrage de valeur 

-- LAG(colonne) -> Renvoie la valeur de la ligne avant la ligne actuelle dans une partition
-- vente	lag(vente)
-- 16000
-- 15000	16000
SELECT annee,nom_vendeur,vente, 
LAG(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

SELECT annee,nom_vendeur,vente, 
vente-LAG(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- LEAD(colonne) -> Renvoie la valeur de la ligne après la ligne actuelle dans une partition
-- vente	lead(vente)
-- 16000	5000
-- 5000		15000
SELECT annee,nom_vendeur,vente, 
LEAD(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- FIRST_VALUE(colonne) ->Renvoie la valeur de l'expression spécifiée par rapport à la première ligne de la frame
SELECT annee,nom_vendeur,vente, 
first_value(vente)  OVER(PARTITION BY nom_vendeur ) FROM ventes;

-- LAST_VALUE(colonne) -> Renvoie la valeur de l'expression spécifiée par rapport à la dernière ligne de la frame
SELECT annee,nom_vendeur,vente, 
last_value(vente)  OVER(PARTITION BY nom_vendeur  ) FROM ventes;

-- NTH_VALUE(colonne,num_ligne) -> Renvoie la valeur de l'argument de la nème ligne
SELECT annee,nom_vendeur,vente, 
NTH_VALUE(vente,4)  OVER(PARTITION BY nom_vendeur  ) FROM ventes;

USE world;
SELECT name,Continent, population,
sum(country.population) OVER(PARTITION BY continent)
FROM country;

SELECT name,Continent, population,
sum(population) OVER(PARTITION BY continent), 
DENSE_RANK() OVER(PARTITION BY continent ORDER BY population DESC) 
FROM country;

USE exemple;
-- FRAME
-- Frame unit -->ROWS
SELECT annee, nom_vendeur ,vente,
sum(vente) OVER (PARTITION BY nom_vendeur
	ORDER BY vente DESC
	ROWS UNBOUNDED PRECEDING 
)
FROM ventes;

SELECT annee, nom_vendeur ,vente,
sum(vente) OVER (PARTITION BY nom_vendeur
	ORDER BY vente DESC
	ROWS 2 PRECEDING 
)
FROM ventes;

SELECT annee, nom_vendeur ,vente,
sum(vente) OVER (PARTITION BY nom_vendeur
	ORDER BY vente DESC
	ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING  
)
FROM ventes;

SELECT annee, nom_vendeur ,vente,
sum(vente) OVER (PARTITION BY nom_vendeur
	ORDER BY vente DESC
	ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING  
)
FROM ventes;

SELECT annee, nom_vendeur ,vente,
sum(vente) OVER (PARTITION BY nom_vendeur
	ORDER BY vente DESC
	RANGE UNBOUNDED PRECEDING  
)
FROM ventes;

-- GROUP BY WITH ROLLUP

USE exemple;

SELECT COALESCE (annee,'Total='),SUM(vente) FROM ventes
GROUP BY annee WITH ROLLUP ;

USE bibliotheque;
SELECT COALESCE (genres.nom,'Total=') AS nom_genre, count(livres.id) AS nb_livre FROM livres
INNER JOIN genres ON livres.genre= genres.id 
GROUP BY genres.nom WITH ROLLUP;

SELECT COALESCE (annee,'Total=') ,COALESCE (genres.nom,'sous-total=') AS nom_genre, count(livres.id) AS nb_livre FROM livres
INNER JOIN genres ON livres.genre= genres.id 
GROUP BY annee,genres.nom WITH ROLLUP;

-- Grouping( ) --> NULL = à 1 sinon égale à 0 
SELECT genres.nom AS nom_genre, count(livres.id) AS nb_livre,GROUPING(genres.nom)  FROM livres
INNER JOIN genres ON livres.genre= genres.id 
GROUP BY genres.nom WITH ROLLUP;
