-- Commentaire fin  de ligne
# Commentaire fin  de ligne

/* 
 * Commentaire
 * sur 
 * plusieurs
 * lignes
 */

-- Créer un base de données exemple
CREATE DATABASE exemple;

-- Afficher l'ensemble des bases de données 
SHOW DATABASES;

-- Choisir la base de données exemple, comme base courante
USE exemple;

-- Supprimer la table article
-- DROP DATABASE exemple;

-- Création d'une table article
CREATE TABLE article(
	reference INT,
	description VARCHAR(255),
	prix DECIMAL(6,2)
);

-- Lister les tables  de la base de données
SHOW tables;

-- Afficher la description de la table (mysql/mariadb)
DESCRIBE article;

-- Supprimer la table
-- DROP TABLE article;

CREATE TABLE vols(
	numero_vol INT,
	heure_depart DATETIME,
	ville_depart VARCHAR(40),
	heure_arrivee DATETIME,
	ville_arrivee VARCHAR(40)
);


-- Modifier une table -> ALTER TABLE
-- Renommer une table (mysql/mariadb)
RENAME TABLE article TO articles; 

-- Ajouter une colonne
ALTER TABLE articles ADD nom VARCHAR(20);

-- Supprimer une colonne 
ALTER TABLE articles DROP description;

-- Modifier le type d'une colonne VARCHAR(20) -> VARCHAR(60)
ALTER TABLE articles MODIFY nom VARCHAR(60);

-- Modifier le nom d'une colonne (nom -> nom_article)
ALTER TABLE articles CHANGE nom nom_article VARCHAR(60);

-- Colonne nullable -> NOT NULL
-- à la création de la table
-- CREATE TABLE article(
-- 	reference INT,
-- 	nom_article VARCHAR(60) NOT NULL,
-- 	prix DECIMAL(6,2)  NOT NULL
-- );

-- Ajouter une NOT NULL sur colonne existante
ALTER TABLE articles MODIFY prix DECIMAL(6,2) NOT NULL;

ALTER TABLE articles MODIFY nom_article VARCHAR(60) NOT NULL;

-- Valeur par défaut DEFAULT 
-- à la création de la table
 -- CREATE TABLE article(
-- 	reference INT,
-- 	nom_article VARCHAR(60) NOT NULL,
-- 	prix DECIMAL(6,2)  NOT NULL DEFAULT 1.0
-- );

-- Modifier une colonne pour ajouter une valeur par défaut 1.0 pour le prix de l'article
ALTER TABLE articles MODIFY prix DECIMAL(6,2) NOT NULL DEFAULT 1.0;

-- Contrainte d’unicité -> UNIQUE
-- à la création de la table
CREATE TABLE stagiaires(
	id INT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	email VARCHAR(150) UNIQUE
);

-- Ajouter une contrainte d'unicité
-- ALTER TABLE stagiaires ADD UNIQUE(email);

-- Ajouter une contrainte nommé d'unicité 
-- ALTER TABLE stagiaires ADD CONSTRAINT un_email_stagiaires UNIQUE (email);

-- Supprimer une contrainte nommé d'unicité
-- ALTER TABLE stagiaires DROP CONSTRAINT un_email_stagiaires;

-- Une clé primaire est la donnée qui permet d'identifier de manière unique une ligne dans une table
-- à la création de la table
-- CREATE TABLE article(
-- 	reference INT PRIMARY KEY,
-- 	nom_article VARCHAR(60) NOT NULL,
-- 	prix DECIMAL(6,2)  NOT NULL DEFAULT 1.0
-- );

-- Modifier la table articles pour que la colonne reférence deviennent la clé primaire de la table
ALTER TABLE articles ADD CONSTRAINT pk_articles PRIMARY KEY(reference);
-- pour une clé primaire le nom de la contrainte n'est pas obligatoir, on pourrait l'écrire aussi :
-- ALTER TABLE articles ADD CONSTRAINT PRIMARY KEY(reference);
ALTER TABLE articles ADD CONSTRAINT pk_articles PRIMARY KEY(reference);

ALTER TABLE stagiaires ADD CONSTRAINT pk_stagiaires PRIMARY KEY(id);

-- AUTO-INCREMENT
-- à la création de la table
-- CREATE TABLE stagiaires(
-- 	id INT PRIMARY KEY AUTO_INCREMENT,
-- 	prenom VARCHAR(40),
-- 	nom VARCHAR(40),
-- 	email VARCHAR(150) UNIQUE
-- );

-- Modifier la colonne référence pour ajouter AUTO_INCREMENT sur la clé primaire
ALTER TABLE stagiaires MODIFY id INT AUTO_INCREMENT; 

-- Modifier la valeur initiale de l'auto increment à 100 par défaut à 1 
ALTER TABLE stagiaires AUTO_INCREMENT=100; 

-- Exercice table pilotes
CREATE TABLE pilotes (
	numero_pilote int PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50)NOT NULL,
	nombre_heure_vol INT NOT NULL
);

-- Exercice table vols
-- Modifier la table vols pour que la colonne numero_vol deviennent la clé primaire de la table
ALTER TABLE vols ADD CONSTRAINT PRIMARY KEY (numero_vol);

-- Ajouter AUTO_INCREMENT sur la clé primaire numero_vol
ALTER TABLE vols MODIFY numero_vol  INT AUTO_INCREMENT ;

-- Ajouter NOT NULL sur les colonnes ville_depart et ville_arrive
ALTER TABLE vols MODIFY ville_depart VARCHAR(40) NOT NULL;
ALTER TABLE vols MODIFY ville_arrivee  VARCHAR(40) NOT NULL;

-- Relation entre table

-- Relation 1,n
CREATE TABLE marques
(
	id INT PRIMARY KEY AUTO_INCREMENT ,
	nom VARCHAR(50) NOT NULL,
	date_creation Date
);

-- On ajoute une colonne qui sera la clé étrangère
ALTER TABLE articles ADD marque INT;

-- Ajout d'une contrainte de clé étrangère 
-- clé étrangère colonne marque de article
-- elle fait référence à la clé primaire id de la table marques
ALTER TABLE articles
ADD CONSTRAINT fk_articles_marques
FOREIGN KEY(marque)
REFERENCES marques(id);

-- Création de la table articles directement avec la contrainte de clé étrangère
-- CREATE TABLE articles(
-- 	reference INT PRIMARY KEY, 
-- 	nom_article VARCHAR(60) NOT NULL,
-- 	prix DECIMAL(6,2)  NOT NULL DEFAULT 1.0
--  marque INT,
--  CONSTRAINT fk_articles_marques
-- 	FOREIGN KEY(marque)
-- 	REFERENCES marques(id);
-- );

-- Exercice: Faire la relation entre vols et pilote et entre vols et avions
CREATE TABLE avions
(
	numero_avion int PRIMARY KEY AUTO_INCREMENT,
	modele varchar(30) NOT NULL,
	capacite int NOT NULL
);

ALTER TABLE vols ADD avions INT;

ALTER TABLE vols ADD pilotes INT;

ALTER TABLE vols ADD CONSTRAINT fk_vols_avions
FOREIGN KEY(avions)
REFERENCES avions(numero_avion);

ALTER TABLE vols ADD CONSTRAINT fk_vols_pilotes
FOREIGN KEY(pilotes)
REFERENCES pilotes(numero_pilote);

-- Relations n-n
CREATE TABLE fournisseurs
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(40) NOT NULL
);

-- Creation de la table de jonction
CREATE TABLE fournisseurs_articles
(
  id_fournisseurs INT,
  id_articles INT,
  
  CONSTRAINT fk_fournisseurs_articles
  FOREIGN KEY (id_fournisseurs)
  REFERENCES fournisseurs(id),
  
  CONSTRAINT fk_articles_fournisseurs
  FOREIGN KEY(id_articles)
  REFERENCES articles(reference),
  
  -- cle primaire composée (le nom est optionnel)
  CONSTRAINT 
  PRIMARY KEY(id_fournisseurs,id_articles)
)

