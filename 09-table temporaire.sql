USE exemple;

CREATE TEMPORARY TABLE utilisateurs
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(100) NOT NULL,
	prenom VARCHAR(100) NOT NULL,
	email VARCHAR(150) NOT NULL
);

SHOW tables;

INSERT INTO utilisateurs(nom,prenom,email) VALUES
('profit','jim','jprofit@dawan.com');

SELECT * FROM utilisateurs;

-- On se deconnecte

SELECT * FROM utilisateurs;

-- table CTE -> table à usage unique
USE bibliotheque;

WITH auteur_vivant_cte AS(
	SELECT id,prenom, nom, naissance FROM auteurs
	WHERE deces IS NULL
)
SELECT prenom, nom, naissance FROM auteur_vivant_cte
WHERE naissance >'1950-01-01';

WITH auteur_vivant_cte AS(
	SELECT auteurs.id,Concat(prenom,' ', auteurs.nom) AS auteur, naissance,
	pays.nom AS nationalite
	FROM auteurs
	INNER JOIN pays ON auteurs.nation=pays.id
	WHERE deces IS NULL
),
auteur_vivant_usa AS(
	SELECT id,auteur, naissance FROM auteur_vivant_cte
	WHERE nationalite='états-unis'
	)
SELECT auteur, naissance FROM auteur_vivant_usa
WHERE naissance >'1948-01-01';	

CREATE TABLE livre_policier
SELECT id,titre,annee FROM livres
WHERE genre=6;

SELECT * FROM livre_policier;

DROP TABLE livre_policier;

CREATE TEMPORARY TABLE auteur_uk
SELECT id, concat(prenom, ' ',nom),naissance 
FROM auteurs
WHERE nation=3;
);

SELECT * FROM auteur_uk;



