USE bibliotheque;

-- DELIMITER $
-- CREATE PROCEDURE auteur_vivant()
-- BEGIN
-- 	SELECT prenom, nom,naissance FROM auteurs WHERE deces IS NULL;
-- END $
-- DELIMITER ;

-- DELIMITER $
-- CREATE PROCEDURE test_variable()
-- BEGIN 
-- 	DECLARE var_local INT DEFAULT 42;
-- 	SELECT var_local;
-- 
-- 	SET var_local=2;
-- 	SELECT var_local;
-- 
-- 	SELECT count(id) INTO var_local FROM auteurs;
-- 	SELECT var_local;
-- 	SELECT @var_global;
-- END $
-- DELIMITER ;


-- DELIMITER $
-- CREATE PROCEDURE addition(IN a INT,IN b INT,OUT somme INT)
-- BEGIN 
-- 	SET somme=a+b;
-- END $
-- DELIMITER ;

-- DELIMITER $
-- CREATE PROCEDURE nb_livre_genre(IN nom_genre VARCHAR(50),OUT nb_livre INT)
-- BEGIN 
-- 	SELECT count(livres.id) INTO nb_livre FROM livres
-- 	INNER JOIN genres ON livres.genre=genres.id 
-- 	WHERE genres.nom=nom_genre;
-- END $
-- DELIMITER ;

-- DELIMITER $
-- CREATE PROCEDURE parite(IN valeur INT,OUT str VARCHAR(10))
-- BEGIN 
-- 		IF valeur MOD 2=0 THEN
-- 			SET str='Paire';
-- 		ELSE
-- 			SET str='Impaire';
-- 		END IF;
-- END $
-- DELIMITER ;

DELIMITER $
CREATE PROCEDURE auteurs_by_livres(IN id_livre INT, OUT auteurs VARCHAR(500))
BEGIN 
	DECLARE done INT DEFAULT 0;
	DECLARE aut VARCHAR(100);

	DECLARE cur_auteur CURSOR FOR 
		SELECT CONCAT(prenom,' ',nom) FROM auteurs 
		INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur
		WHERE livre2auteur.id_livre=id_livre;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=1;
	OPEN cur_auteur;
	SET auteurs ='';
	WHILE done=0 DO
		FETCH cur_auteur INTO aut;
			IF done=0 THEN
				SET auteurs=CONCAT(auteurs,aut);
			END IF;
	END WHILE;
	CLOSE cur_auteur;
	
END $
DELIMITER ;

-- DROP PROCEDURE auteur_vivant;