USE exemple;

CREATE TABLE compte_bancaire(
	id INT PRIMARY KEY AUTO_INCREMENT,
	solde DECIMAL(10,3),
	iban VARCHAR(20),
	titulaire VARCHAR(50)
);

INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES
(1000.000,'FR-105-0000-01','John Doe'),
(5000.000,'FR-105-0000-02','Jane Doe'),
(500.000,'FR-105-0000-03','Alan Smithee'),
(400.000,'FR-105-0000-04','Yves Roulo'),
(4000.000,'FR-105-0000-05','Jo Dalton');

-- Atomicité
-- désactiver l'autocommit
SET autocommit=0;

START TRANSACTION; -- Commencer une transaction
-- transfert entre 2 comptes
UPDATE compte_bancaire SET solde=solde-200.0 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+200.0 WHERE id=2;
COMMIT; -- valider la TRANSACTION

SELECT * FROM compte_bancaire;

START TRANSACTION; -- Commencer une transaction
-- transfert entre 2 comptes
UPDATE compte_bancaire SET solde=solde-50.0 WHERE id=2;
ROLLBACK;  -- annuler la transaction

SELECT * FROM compte_bancaire;

START TRANSACTION; -- Commencer une transaction
-- transfert entre 2 comptes
UPDATE compte_bancaire SET solde=solde-300.0 WHERE id=3;
UPDATE compte_bancaire SET solde=solde+300.0 WHERE id=4;
SAVEPOINT transfert1;
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES
(100.000,'FR-105-0000-09','Antoine beretto');
ROLLBACK TO transfert1;
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES
(1000.000,'FR-105-0000-09','Antoine beretto');
SAVEPOINT creation_compte_beretto;
UPDATE compte_bancaire SET solde=solde-500.0 WHERE id=2;
UPDATE compte_bancaire SET solde=solde+500.0 WHERE id=7;

-- libérer les points de sauvegardes
RELEASE SAVEPOINT creation_compte_beretto;
RELEASE SAVEPOINT transfert1;
COMMIT;

SET autocommit =1;