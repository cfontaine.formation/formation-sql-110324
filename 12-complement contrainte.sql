USE exemple;

-- CHECK -> permet de limiter la plage de valeur
CREATE TABLE personnes
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom  VARCHAR(50) NOT NULL CHECK(CHAR_LENGTH(nom)>=2 AND nom RLIKE '^[a-z A-Z]+$'),
	age INT NOT NULL CHECK(age>0)
);

INSERT INTO personnes(prenom,nom, age) VALUE ('John','Doe',36);

INSERT INTO personnes(prenom,nom, age) VALUE ('Jane','D',34);

INSERT INTO personnes(prenom,nom, age) VALUE ('Jane','Doe',-34);

INSERT INTO personnes(prenom,nom, age) VALUE ('Jane','Doe34',34);

INSERT INTO personnes(prenom,nom, age) VALUE ('Jane','Doe',34);

DROP TABLE personnes;

-- CONSTRAINT pour un check sur plusieurs colonnes
CREATE TABLE personnes
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom  VARCHAR(50) NOT NULL CHECK(CHAR_LENGTH(nom)>=2 AND nom RLIKE '^[a-z A-Z]+$'),
	age INT NOT NULL,
	pays VARCHAR(40) NOT NULL,
	CONSTRAINT CHK_personne CHECK((age>=13 AND pays='france')OR(age>10 AND pays <> 'france'))
);



INSERT INTO personnes(prenom,nom, age,pays) VALUE ('Jane','Doe',12,'Espagne');

INSERT INTO personnes(prenom,nom, age,pays) VALUE ('Jane','Doe',14,'France');

INSERT INTO personnes(prenom,nom, age,pays) VALUE ('Jo','Dalton',9,'Espagne');

INSERT INTO personnes(prenom,nom, age,pays) VALUE ('Jo','Dalton',12,'France');

-- ON DELETE et ON UPDATE
CREATE TABLE marques_cascade(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	date_creation DATE
);

CREATE TABLE articles_cascade(
	reference INT PRIMARY KEY AUTO_INCREMENT,
	description VARCHAR(255),
	prix DECIMAL(6,2) NOT NULL DEFAULT 10.0, 				
	marque INT,
	CONSTRAINT  FK_articles_marques_cascade
	FOREIGN KEY (marque)
	REFERENCES marques_cascade(id)
);

INSERT INTO marques_cascade(nom) VALUES 
('Marque A'),
('Marque B'),
('Marque C');


INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,1),
('Clavier AZERTY',20.0,1),
('TV 4K',600.0,2),
('Carte mère ',130.0,3),
('Carte graphique',500.0,3);

-- DELETE FROM marques_cascade WHERE id=1;

-- ON DELETE CASCADE
ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON DELETE CASCADE;

DELETE FROM marques_cascade WHERE id=1;

INSERT INTO marques_cascade(nom) VALUES ('Marque A');
INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,4),
('Clavier AZERTY',20.0,4);

-- ON DELETE SET NULL
ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON DELETE SET NULL;

DELETE FROM marques_cascade WHERE id=4;

-- ON UPDATE CASCADE

INSERT INTO marques_cascade(nom) VALUES ('Marque A');
UPDATE articles_cascade SET marque=5
WHERE reference=6 OR reference=7;

ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON UPDATE CASCADE;

UPDATE marques_cascade SET id=10 WHERE id=5;

-- ON UPDATE SET NULL
ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON UPDATE SET NULL;

UPDATE marques_cascade SET id=20 WHERE id=10;
