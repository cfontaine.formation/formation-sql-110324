USE exemple;

CREATE TABLE compte_bancaire(
	id INT PRIMARY KEY AUTO_INCREMENT,
	solde DECIMAL(10,3),
	iban VARCHAR(20),
	titulaire VARCHAR(50)
);

INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES
(1000.000,'FR-105-0000-01','John Doe'),
(5000.000,'FR-105-0000-02','Jane Doe'),
(500.000,'FR-105-0000-03','Alan Smithee'),
(400.000,'FR-105-0000-04','Yves Roulo'),
(4000.000,'FR-105-0000-05','Jo Dalton');
