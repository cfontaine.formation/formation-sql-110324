USE hr;

SELECT * FROM countries 
INNER JOIN locations ON locations.country_id  =countries.country_id;

SELECT * FROM countries 
INNER JOIN locations ON locations.country_id  =countries.country_id
WHERE country_name='United Kingdom';

SELECT * FROM countries
INNER join locations ON countries.country_id=locations.country_id
INNER join regions ON countries.region_id=regions.region_id
WHERE regions.region_name='Americas';

SELECT locations.country_id, countries.country_name FROM countries
LEFT JOIN locations ON countries.country_id =locations.country_id
WHere locations.country_id IS NULL;

SELECT CONCAT(employees.first_name,' ', employees.last_name) AS employe, department_name
FROM employees
LEFT JOIN departments ON departments.department_id =employees.department_id WHERE department_name='finance' ;

SELECT regions.region_name  FROM countries
INNER JOIN locations ON countries.country_id =locations.country_id
RIGHT JOIN  regions ON countries.region_id=regions.region_id
WHERE locations.country_id IS NULL;

SELECT employees.first_name, employees.last_name
FROM employees
INNER JOIN departments  ON employees.department_id=departments.department_id
INNER JOIN locations ON departments.location_id=locations.location_id
INNER JOIN countries ON locations.country_id=countries.country_id
WHERE country_name='canada';

SELECT *
FROM employees
INNER JOIN departments  ON employees.department_id=departments.department_id
INNER JOIN locations ON departments.location_id=locations.location_id
INNER JOIN countries ON locations.country_id=countries.country_id;
WHERE country_name='canada';