CREATE DATABASE pizzeria;

USE pizzeria;

CREATE TABLE pizzas(
 	numero_pizza int PRIMARY KEY AUTO_INCREMENT,
 	nom VARCHAR(40) NOT NULL,
 	base ENUM('rouge','blanche','rose') NOT NULL,
 	prix DECIMAL(4,2) NOT NULL,
 	photo BLOB
);

CREATE TABLE ingredients(
 	numero_ingredient int PRIMARY KEY AUTO_INCREMENT,
 	nom VARCHAR(40) NOT NULL
);

CREATE TABLE pizzas_ingredients(
	numero_pizza INT,
	numero_ingredient INT,
	
	CONSTRAINT fk_pizzas_ingredients
	FOREIGN KEY(numero_pizza)
	REFERENCES pizzas(numero_pizza),
	
	CONSTRAINT fk_ingredients_pizzas
	FOREIGN KEY (numero_ingredient)
	REFERENCES ingredients(numero_ingredient),
	
	CONSTRAINT
	PRIMARY KEY (numero_pizza,numero_ingredient)
);

-- suite
CREATE TABLE commandes
(
	numero_commandes INT PRIMARY KEY AUTO_INCREMENT ,
	heure_commande DATETIME NOT NULL,
	heure_livraison DATETIME NOT NULL
);

CREATE TABLE pizzas_commandes(
	numero_pizza INT,
	numero_commandes INT,
	
	quantite INT NOT NULL,
	
	CONSTRAINT fk_pizzas_commandes
	FOREIGN KEY(numero_pizza)
	REFERENCES pizzas(numero_pizza),
	
	CONSTRAINT fk_commandes_pizzas
	FOREIGN KEY (numero_commandes)
	REFERENCES commandes(numero_commandes),
	
	CONSTRAINT
	PRIMARY KEY (numero_pizza,numero_commandes)
);

