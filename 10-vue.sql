USE bibliotheque;

CREATE VIEW v_livre_genre AS
SELECT livres.id,titre,annee,genres.nom AS categorie,
	   concat(auteurs.prenom, ' ',auteurs.nom) AS auteur
FROM livres
INNER JOIN genres ON livres.genre = genres.id 
INNER JOIN livre2auteur ON livres.id =livre2auteur.id_livre 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id;

SHOW tables;

SELECT titre,auteur,categorie FROM v_livre_genre WHERE annee =1954;

INSERT INTO livres (titre,annee,genre) VALUES ('Les misérables',1862,7);

INSERT INTO livre2auteur(id_livre,id_auteur) VALUES (143,33); 

-- la vue est mise à jour automatiquement
SELECT * FROM v_livre_genre;

-- DROP VIEW v_livre_genre;

-- Modifier une vue (recréer)
ALTER VIEW v_livre_genre AS
SELECT livres.id,titre,annee,genres.nom AS categorie,
	   concat(auteurs.prenom, ' ',auteurs.nom) AS auteur, pays.nom AS nom_pays
FROM livres
INNER JOIN genres ON livres.genre = genres.id 
INNER JOIN livre2auteur ON livres.id =livre2auteur.id_livre 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id
INNER JOIN pays ON pays.id = auteurs.nation ;

SELECT * FROM v_livre_genre;